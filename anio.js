// var app = require('express')();
// var http = require('http').Server(app);
// var io = require('socket.io')(http);

// var cors = require('cors');
// app.use(cors());

// io.on('connection', function(socket){

// 	console.log('a user connected');

// 	socket.on('disconnect', function() {
// 		console.log('disconnect');
//    	});

// 	socket.on('pesan', function(msg){
// 		console.log(msg);
// 		setTimeout(function(){
// 			socket.emit('balas', {
// 				datax: 'balasan ' + msg
// 			});
// 		}, 1000)
// 	});

// });

// var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
// var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
 
// http.listen(server_port, server_ip_address, function () {
//   console.log( "Listening on " + server_ip_address + ", server_port " + server_port )
// });

var http = require('http');
var express = require('express');
var app = express();

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1');

http.createServer(app).listen(app.get('port'), app.get('ip'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});